// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort()

for(var i = 0; i < daftarHewan.length; i++) {
    console.log(daftarHewan[i]);
}

console.log("...jawaban soal 1")
console.log()

//soal 2
function introduce(name, age, address, hobby){
    name = "John"
    age = "30"
    address = "Jalan Pelesiran"
    hobby = "Gaming"

    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
}

var data = {name : "John", age : "30", address : "Jalan Pelesiran", hobby : "Gaming"}

var perkenalan = introduce(data)
console.log(perkenalan)

console.log("...jawaban soal 2")
console.log()

// soal 3
function hitung_huruf_vokal(vokal) {
    panjangKata = vokal.length
    jumlahVokal = 0
    for(i = 0; i < panjangKata; i++) {
        if(vokal.charAt(i) == "a" || vokal.charAt(i) == "A" || vokal.charAt(i) == "i" || vokal.charAt(i) == "I" || vokal.charAt(i) == "u" || vokal.charAt(i) == "U" || vokal.charAt(i) == "e" || vokal.charAt(i) == "E" || vokal.charAt(i) == "o" || vokal.charAt(i) == "O"){
            jumlahVokal += 1
        }
    }
    return jumlahVokal
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2)

console.log("...jawaban soal 3")
console.log()

// soal 4
function hitung(angka) {
    hasil = -2
    for(i = 0; i < angka; i ++){
        hasil += 2
    }

    return hasil
}

console.log(hitung(0))
console.log(hitung(1))
console.log(hitung(2))
console.log(hitung(3))
console.log(hitung(5))

console.log("...jawaban soal 4")