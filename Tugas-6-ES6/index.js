// soal 1
const persegipanjang = (panjang, lebar) => {
    luas = panjang * lebar
    keliling = 2 * (panjang + lebar)

    return "Luas = " + luas + ", Keliling = " + keliling
}

console.log(persegipanjang(12, 100))
console.log("...jawaban soal 1")

console.log()

// soal 2
const newFunction = (firstName, lastName) => {
    return firstName + " " + lastName
}

console.log(newFunction("William", "Imoh"))
console.log("...jawaban soal 2")

console.log()

// soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)
console.log("...jawaban soal 3")

console.log()

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]
console.log(combined)
console.log("...jawaban soal 4")

console.log()

// soal 5
const planet = "earth" 
const view = "glass"

const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`
console.log(before)
console.log("...jawaban soal 5")