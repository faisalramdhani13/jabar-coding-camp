var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

function baca(sisaWaktu, books, i) {
    readBooksPromise(sisaWaktu, books[i], function(time){
        const nextBooks = i += 1
        if(nextBooks < books.length) {
            baca(time, books, nextBooks)
        }
    })
}
baca(10000, books, 0)