// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar JAVASCRIPT itu keren";
// jawaban soal 1
var gabung = pertama.substr(0, 4) + " " + pertama.substr(12, 6) + " " + kedua.substr(0, 18);
console.log(gabung);

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
// jawaban soal 2
var kataPertama = 10;
var kataKedua = 2;
var kataKetiga = 4;
var kataKeempat = 6;
console.log((kataPertama - kataKedua - kataKetiga) * kataKeempat);

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 
// jawaban soal 3
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(14, 18);
var kataKeempat = kalimat.substring(18, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);