// soal 1
var nilai = 70
if (nilai >= 85) {
    console.log("A")
} else if (nilai >=75 && nilai < 85) {
    console.log("B")
} else if (nilai >= 65 && nilai < 75) {
    console.log("C")
} else if (nilai >= 55 && nilai < 65) {
    console.log("D")
} else if (nilai < 55) {
    console.log("E")
}

console.log("...jawaban soal 1")
console.log ()
// soal 2
var tanggal = 12;
var bulan = 11;
var tahun = 2001;
switch(bulan) {
    case 1 : {console.log ("12 Januari 2001"); break;}
    case 2 : {console.log ("12 Febuari 2001"); break;}
    case 3 : {console.log ("12 Maret 2001"); break;}
    case 4 : {console.log ("12 April 2001"); break;}
    case 5 : {console.log ("12 Mei 2001"); break;}
    case 6 : {console.log ("12 Juni 2001"); break;}
    case 7 : {console.log ("12 Juli 2001"); break;}
    case 8 : {console.log ("12 Agustus 2001"); break;}
    case 9 : {console.log ("12 September 2001"); break;}
    case 10 : {console.log ("12 Oktober 2001"); break;}
    case 11 : {console.log ("12 November 2001"); break;}
    case 12 : {console.log ("12 Desember 2001"); break;}
    default : {console.log ("Bulan yang anda masukkan salah"); break;}
}

console.log("...jawaban soal 2")
console.log()

// soal 3
var n = 3;
var m = "";
for(i = 1; i <= n; i++) {
    for(j = 1; j <= i; j++){
        m += "#"
    }
    m += "\n";
}
console.log(m);

console.log("...jawaban soal 3")
console.log()

// soal 4
var m = 10;

switch (m) {
    case 1 : {console.log("1 - I love programming"); break;}
    case 2 : {console.log("1 - I love programming"); console.log("2 - I love Javascript"); break;}
    case 3 : {console.log("1 - I love programming"); console.log("2 - I love Javascript"); console.log("3 - I love VueJS"); console.log("==="); break;}
    case 4 : {console.log("1 - I love programming"); console.log("2 - I love Javascript"); console.log("3 - I love VueJS"); console.log("==="); console.log("4 - I love programming"); break;}
    case 5 : {console.log("1 - I love programming"); console.log("2 - I love Javascript"); console.log("3 - I love VueJS"); console.log("==="); console.log("4 - I love programming"); console.log("5 - I love Javascript"); break;}
    case 6 : {console.log("1 - I love programming"); console.log("2 - I love Javascript"); console.log("3 - I love VueJS"); console.log("==="); console.log("4 - I love programming"); console.log("5 - I love Javascript"); console.log("6 - I love VueJS"); console.log("======"); break;}
    case 7 : {console.log("1 - I love programming"); console.log("2 - I love Javascript"); console.log("3 - I love VueJS"); console.log("==="); console.log("4 - I love programming"); console.log("5 - I love Javascript"); console.log("6 - I love VueJS"); console.log("======"); console.log("7 - I love programming"); break;}
    case 8 : {console.log("1 - I love programming"); console.log("2 - I love Javascript"); console.log("3 - I love VueJS"); console.log("==="); console.log("4 - I love programming"); console.log("5 - I love Javascript"); console.log("6 - I love VueJS"); console.log("======"); console.log("7 - I love programming"); console.log("8 - I love Javascript"); break;}
    case 9 : {console.log("1 - I love programming"); console.log("2 - I love Javascript"); console.log("3 - I love VueJS"); console.log("4 - I love programming"); console.log("5 - I love Javascript"); console.log("6 - I love VueJS"); console.log("======"); console.log("7 - I love programming"); console.log("8 - I love Javascript"); console.log("9 - I love VueJS"); console.log("========="); break;}
    case 10 : {console.log("1 - I love programming"); console.log("2 - I love Javascript"); console.log("3 - I love VueJS"); console.log("==="); console.log("4 - I love programming"); console.log("5 - I love Javascript"); console.log("6 - I love VueJS"); console.log("======"); console.log("7 - I love programming"); console.log("8 - I love Javascript"); console.log("9 - I love VueJS"); console.log("========="); console.log("10 - I love programming"); break;}
}

console.log("...jawaban soal 4");